#!/usr/bin/env python2

import picoscope
from picoscope import ps3000a
import matplotlib.pyplot as plt
import numpy as np
import time
from ctypes import *

# tracing context
tc = {}
tc["collection_title"] = "PLC waterlevel normal"
tc["collection_description"] = "PLC waterlevel program, capture date 2017-05-27"
tc["collection_input_range"] = range(0,16)
tc["traces_per_traceset"] = 20000
tc["sample_interval"] = 1e-9 # 1 / sampling rate: 1GS/s -> 1e-9
tc["trace_duration"] = 60000e-9 # Works out to 60000 samples / trace.
tc["capture_channel"] = "A"
tc["voltage_range"] = 0.1
tc["coupling"] = "DC"
tc["trigger_voltage"] = 0.6
# How many traces do you need per rapid block call?
# Keep memory constraints of the device in mind.
tc["traces_per_rapid_block_call"] = 4000
tc["scope_serial"] = "CO130/005"

# Do we want to truncate the end of the trace? Since the trigger happens
# at the end, about 35000 samples are superfluous. Truncation makes
# trace size more manageable.
tc["truncate"] = 35000


def create_header(scope_context,
                  num_traces, num_samples, sample_coding, global_title,
                  description, x_label, y_label, x_scale,
                  v_range, coupling, scope_id):

    # Header is documented in appendix K of the inspector user manual.

    SAMPLE_CODING_FLOAT = 0b00010000
    SAMPLE_CODING_BYTE =  0b00000000
    SAMPLE_CODING_LENGTH_1 = 0b00000001
    SAMPLE_CODING_LENGTH_2 = 0b00000010
    SAMPLE_CODING_LENGTH_4 = 0b00000100

    #TODO: implement sample coding.

    # Number of traces (mandatory)
    header = bytearray()
    header.extend(create_header_field(0x41, c_uint32(num_traces)))
    # Number of samples per trace (mandatory)
    header.extend(create_header_field(0x42, c_uint32(num_samples)))
    # Sample encoding (float / byte, length per sample) (mandatory)
    # Note that we're shifting down to single bytes!
    header.extend(
        create_header_field(0x43, SAMPLE_CODING_BYTE | SAMPLE_CODING_LENGTH_1))
    # Global trace title
    header.extend(create_header_field(0x46, global_title))
    # Global description
    header.extend(create_header_field(0x47, description))
    # X axis label
    header.extend(create_header_field(0x49, x_label))
    # Y axis label
    header.extend(create_header_field(0x4a, y_label))
    # X axis scaling
    header.extend(create_header_field(0x4b, c_float(x_scale)))
    # Y axis scaling
    # Multiply by 256 because of the downshift to single bytes
    header.extend(create_header_field(0x4c, c_float(
        scope_context.getScaleAndOffset("A")["scale"] * 256)))
    # Scope range
    header.extend(create_header_field(0x55, c_float(v_range)))
    # Scope coupling
    header.extend(create_header_field(
        0x56, c_uint32(scope_context.CHANNEL_COUPLINGS[coupling])))
    # Scope offset
    # TODO is this influenced by the downshift?
    header.extend(create_header_field(0x57, c_float(
        scope_context.getScaleAndOffset("A")["offset"])))
    # Scope ID
    header.extend(create_header_field(0x59, scope_id))
    # Header end
    header.append(0x5f)
    header.append(0x00)
    return header


def create_header_field(tag, valuebytes):
    header = bytearray()
    try:
        length = len(valuebytes)
    except TypeError as e:
        if isinstance(valuebytes, c_uint32) or isinstance(valuebytes, c_float):
            length = 4
        elif isinstance(valuebytes, int):
            length = 1
            valuebytes = [valuebytes]
        else:
            raise e

    header.append(tag)
    if length >= 128:
        header.append(0b10000000 | 4)
        header.extend(c_uint32(length))
    else:
        header.append(length)
    header.extend(valuebytes)
    return header


def capture_run_start(scope_context):
    t1 = time.time()
    # Start the collection in rapid block mode and wait until done.
    scope_context.runBlock(pretrig=1.0)
    return t1

def capture_run_finish(scope_context, n_traces, n_samples, start_time):
    # Prepare memory to receive the samples.
    data = np.zeros((n_traces, n_samples), dtype=np.int16)
    scope_context.waitReady()

    t2 = time.time()
    # Very inaccurate, since ps.waitReady waits with time.sleep(0.01)
    print "Elapsed time since start of sampling: ", str(t2 - start_time)

    # Retrieve the data. Handles all the heavy lifting such as setting the data
    # areas for the library to write to.
    # FIXME implement channel selection based on tc.
    scope_context.getDataRawBulk(data=data, numSamples=n_samples)

    t3 = time.time()
    print "Time to read data: ", str(t3 - t2)

    # Check whether only the upper 8 bits are used
    if np.any(np.left_shift(data, 8)):
        # Not safe to shift down to single bytes, abort!
        raise ValueError("Precision would be lost by shift, aborting")

    data = np.right_shift(data, 8).astype(np.int8)
    return data



def capture_traceset(scope_context, scope_serial,
                     traceset_filename, traceset_title, traceset_description,
                     traces_per_traceset, traces_per_rapid_block_call,
                     samples_per_trace, sample_interval, voltage_range,
                     coupling, truncate, **superfluous_args):
    tstart = time.time()
    with open(traceset_filename, "wb") as output_file:
        # Write the header
        output_file.write(create_header(scope_context,
                                        traces_per_traceset,
                                        samples_per_trace - truncate,
                                        0, # FIXME sample coding not implemented
                                        traceset_title,
                                        traceset_description,
                                        "s",
                                        "V",
                                        sample_interval,
                                        voltage_range,
                                        coupling,
                                        scope_serial
                                        ))

        remaining_traces = traces_per_traceset
        start_time = capture_run_start(scope_context)
        data = capture_run_finish(scope_context, traces_per_rapid_block_call,
                                  samples_per_trace - truncate, start_time)
        while remaining_traces > traces_per_rapid_block_call:
            start_time = capture_run_start(scope_context)
            t1 = time.time()
            output_file.write(data)
            t2 = time.time()
            print "Time to write data to disk: ", str(t2 - t1)
            data = capture_run_finish(scope_context, traces_per_rapid_block_call,
                                      samples_per_trace - truncate, start_time)
            remaining_traces -= traces_per_rapid_block_call

        t1 = time.time()
        output_file.write(data[0:remaining_traces])
        t2 = time.time()
        print "Time to write data to disk: ", str(t2 - t1)

    tend = time.time()
    print "Total time to capture traceset: ", str(tend - tstart)
    return scope_context

    #plt.imshow(data[:, 0:ps.noSamples], aspect='auto', interpolation='none',
    #	cmap=plt.cm.hot)
    #dataV = ps.rawToV(channel="A", dataRaw=data[1])
    #print ps.noSamples
    #print dataV
    #print len(dataV)
    #plt.plot(dataV)
    #plt.show()


def configure_scope(scope_serial, capture_channel, coupling, voltage_range,
                    sample_interval, trace_duration,
                    traces_per_rapid_block_call, trigger_voltage,
                    **superfluous_args):
    #picoscope = reload(picoscope)
    #ps3000a = reload(ps3000a)

    scope_serial += '\x00'
    scope_context = ps3000a.PS3000a(scope_serial)

    # rapid block mode
    # Configure the sampling channel.
    voltage_range =  scope_context.setChannel(channel=capture_channel,
                                   coupling=coupling,
                                   VRange=voltage_range)
    # Can go lower to 0.05.

    # Explicitly disable every channel you don't need; the scope will not
    # work in its fastest mode unless you do this.
    if capture_channel == "A":
        scope_context.setChannel(channel="B", enabled=False)
    elif channel == "B":
        scope_context.setChannel(channel="A", enabled=False)

    # Configure the scope for tracing.
    sample_interval, no_samples_per_trace, max_samples_per_trace = \
        scope_context.setSamplingInterval(sample_interval, trace_duration)

    # Set a trigger on the external channel.
    scope_context.setSimpleTrigger("External", trigger_voltage)

    # Configure the scope's memory to be segmented. Return value is the actual
    # maximum number of samples in a segment, for all channels combined. I.e.
    # when using 2 channels, the number of samples available per channel is
    # samples_per_segment / 2.
    max_samples_per_trace = \
        scope_context.memorySegments(traces_per_rapid_block_call)
    if no_samples_per_trace > max_samples_per_trace:
        raise RuntimeError("Unable to fit traces in memory! Reduce number of "
                           "traces per rapid block call.")

    # Set the number of captures that will be taken in one run of rapid block mode.
    # This should not fail, since if the requested number of captures is too many
    # based on the sample interval and duration, memorySegments should have failed.
    scope_context.setNoOfCaptures(traces_per_rapid_block_call)

    return scope_context, voltage_range, sample_interval, no_samples_per_trace


if __name__ == "__main__":
    (tc["scope_context"], tc["voltage_range"],
     tc["sample_interval"], tc["samples_per_trace"]) = configure_scope(**tc)

    for plc_input in tc["collection_input_range"]:
        raw_input("Set PLC input to {} and press return".format(plc_input))
        tc["traceset_title"] = \
            "{} - {}".format(tc["collection_title"], plc_input)
        tc["traceset_description"] = \
            "{}, input {}".format(tc["collection_description"], plc_input)
        tc["traceset_filename"] = "{}.trs".format(tc["traceset_title"])
        tc["scope_context"] = capture_traceset(**tc)
    # Finish by closing the scope.
    tc["scope_context"].close()
