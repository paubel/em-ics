

clear all;
close all;

%INPUT PARAMETERS -----
no_traces=50000; 
no_samples=1500; %all samples of the trace
sample_interval=[1:1500];  %interval of choice for the full trace
interval_size=sample_interval(length(sample_interval))-sample_interval(1)+1;
offset=0; %trs file offset
data_size=0; %relates to the IO
train_size=10000;  %train set and test set sizes
test_size=5000;
no_inputs=6; %number of files

no_subintervals=1; %number of intervals in which we perform dimensionality reduction
%Define the chosen intervals here:
sub_int(:,1)=[1,1500];
sub_int(:,2)=[600,1000];
sub_int(:,3)=[1050,1200];

m=30; %number of POIs to select via PCA/LDA per subinterval
d=500; %number of traces used in detection: Make test_size divisible by d
red_mode=2; % 1 for PCA, 2 for LDA 
% Quickly try both

%see : [https://www.cl.cam.ac.uk/~mgk25/cardis2013-templates.pdf]
comp_mode=2; % 1 for naive probability computation, 2 for Choudary improvemnets ( use 2 )
% Always 2

comb_mode=2; % 1 for Averaging attack traces, 2 for using the joint likelihood (1==2 with dLINEAR)
% Always use joint likelihood (2)
ch_method=1; % 1 for Choudary MD discriminant, 2 for Choudary LINEAR discriminant
% Always 1

%Note: looking at Wagner et al. [https://eprint.iacr.org/2016/1155.pdf] I
%don't see much more than Choudary. Good performance is achieved by dLINEAR
%via an averaged covariance matrix. A useful point of Wagner is weighted
%variants if the leakages have no uniform priors. 
%END OF INPUT -------




%File import
for i=1:no_inputs

    index_str = num2str(i-1);
    filename='/home/macgyver/radboud/phd-student/research/EM/data/plc';
    suffix='.trs';
    filename=strcat(filename,index_str);
    filename=strcat(filename,suffix);
    [fileID,status]=open_trs(filename,no_traces,no_samples,data_size,offset);
    [traces, data]=read_trs(fileID,status,no_traces,no_samples,data_size);
    close_trs(fileID);

    train_set(:,:,i)=traces(1:train_size,sample_interval);
    test_set(:,:,i)=traces((train_size+1):(train_size+test_size),sample_interval);
    clear traces;
    
end

%Dimensionality reduction

projection=cell(no_subintervals,1);
for si=1:no_subintervals
    
    clear mean_op; %clear them for every subinterval 
    clear mean_total;
    clear c_full;
    clear c_pool_full;
    
    %mean
    for i=1:no_inputs   
        mean_op(:,i)=mean(train_set(:,sub_int(1,si):sub_int(2,si),i));
    end
    %mean of all operations
    mean_total= mean(mean_op,2);
    
    %c_pool used in LDA
    for i=1:no_inputs
        c_full(:,:,i) = cov(train_set(:,sub_int(1,si):sub_int(2,si),i));
    end
    c_pool_full = (1/no_inputs)*sum(c_full,3);

    %B used in both PCA, LDA
    B=zeros(size(mean_total,1));
    for i=1:no_inputs
        B = B+(mean_op(:,i)-mean_total)*(mean_op(:,i)-mean_total)';
    end

    B=train_size*B;

    if (red_mode==1) %PCA method
        [U,S,V] = svd(B);
        Ur=U(:,1:m);
    end
    
    if (red_mode==2) %LDA method
       %c1= c_pool_full^(-1/2);
       %[U,S,V] = svd(c1*B*c1);
      [U,S,V] = svd(inv(c_pool_full)*B);
       Ur=U(:,1:m);
    end
    
    
   %The following scree graph shows the elbow 
   %plot(sum(S));
   %cummulative percentage of total variation
   delta=sum(S);
   phi(si)= sum(delta(1:m))/ sum(delta(1:(1:(sub_int(:,2)-sub_int(:,1)+1)))); 
   
   %Store Ur
   projection{si}=Ur;
   
end




%Template Building
for i=1:no_inputs
    full_projected_train=[];
    full_projected_test=[];
    for si=1:no_subintervals
        %project every interval 
        projected_train=(projection{si}'*train_set(:,sub_int(1,si):sub_int(2,si),i)')';
        projected_test=(projection{si}'*test_set(:,sub_int(1,si):sub_int(2,si),i)')';
        full_projected_train=horzcat(full_projected_train,projected_train);
        full_projected_test=horzcat(full_projected_test,projected_test);
        
    end
        me(:,i)= mean(full_projected_train,1);
        
        c(:,:,i) = cov(full_projected_train);
   
        set(:,:,i)=  full_projected_test;
       
       c_dec(:,i)=log(diag(chol(c(:,:,i)))); %cholesky decomposition
        
end


    c_pool = (1/no_inputs)*sum(c,3); %is this the correct way? I think so.

    

%Match rate estimation for all inputs/keys/operations
for i=1:no_inputs
    counter(i)=0; %counts the misclassified cases
    
    %partition the test_set into d-sized components
    %i.e. for all d-sized matrices of the test set
    for j=1:test_size/d

    %Computing the denominator is NOT necessary 

%Direct score computation via mvnpdf
% dFULL + MULT (FULL implies probability computation)
if (comp_mode==1)
        for k=1:no_inputs
            nom=1;
            %Combination via multiplication
            for t=1:d
                nom=nom*mvnpdf((set((j-1)*d+t,:,i)), me(:,k)',c(:,:,k));
            end
            %p(k)=log(nom/denom); 
            p(k)=log(nom); 
            if ((max(p)==-Inf) | (max(p)==Inf))
               disp('To Infinity and beyond!');
            end
        end
end
     
%Choudary improvements
if (comp_mode==2)
    for k=1:no_inputs
        
        %dLOG + AVG, i.e. cholesky decomposition for determinant computation and
        %direct inversion. Average the detection traces before matching
       
%         t1=-log(sum(c_dec(:,k)));
%         trace=set( ((j-1)*d+1):((j-1)*d+d),:,i);
%         trace=mean(trace);
%         %Compute the matrix inversion
%         t2=-0.5*(trace-me(:,k)')*inv(c(:,:,k))*(trace-me(:,k)')';
%         p(k)=t1+t2;
        
         %attack traces
         trace=set( ((j-1)*d+1):((j-1)*d+d),:,i);
         %Inverted pooled cov matrix after dim reduction
         inv_c=inv(c_pool);
         %dMD+AVG
        if (comb_mode==1) &&(ch_method==1)           
            mean_trace=mean(trace);
            p(k)=-0.5*(mean_trace-me(:,k)')*inv_c*(mean_trace-me(:,k)')';
        end
        %dMD+COMB
        if (comb_mode==2)&&(ch_method==1)  
            p(k)=0;
            for index=1:d
                p(k)=p(k)+(trace(index,:)-me(:,k)')*inv_c*(trace(index,:)-me(:,k)')';
            end
            p(k)=-0.5*p(k);
        end
        
        %dLINEAR + AVG
        if (comb_mode==1) &&(ch_method==2)    
            mean_trace=mean(trace);
            p(k)= me(:,k)'*inv_c*mean_trace' - 0.5*me(:,k)'*inv_c*me(:,k);
        end
        %dLINEAR + COMB
        if (comb_mode==2) &&(ch_method==2)    
            p(k)=0;
            for index=1:d
                p(k)=p(k)+me(:,k)'*inv_c*sum(trace)' - (d/2)*me(:,k)'*inv_c*me(:,k);;
            end
        end
         
          
      
    end
    scores(j,i,:)=p;
end


if ((p(i)<max(p))| (max(p)==-Inf)| (max(p)==Inf))    % careful here with Inf
    counter(i)=counter(i)+1;
end

end
end

for i=1:no_inputs
    success_rate(i)=1-counter(i)/(test_size/d);
end

success_rate



