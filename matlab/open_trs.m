
function [trs_fileID,status]=open_trs(filename,no_traces,no_samples,data_size,offs)



offset = offs ;

trs_fileID = fopen(filename) ;

traces  = single(zeros( no_traces , no_samples )) ;
data = uint8(zeros( no_traces , data_size )) ; 

status = fseek(trs_fileID, offset, 'bof');



end