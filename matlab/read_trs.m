
function [traces, data]=read_trs(trs_fileID,status,no_traces,no_samples,data_size)


traces  = (zeros( no_traces , no_samples )) ;

data  = (zeros( no_traces , data_size )) ;


for i = 1:no_traces,
     status = fseek(trs_fileID, 0, 'cof'); % there is an additional header before each trace
   
    
    
    
        traces(i,:) = (fread(trs_fileID,no_samples,'int8'));
       
     
end

 

end
