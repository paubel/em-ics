#!/usr/bin/env python3

import numpy as np
import scipy as sp
import matplotlib
from scipy import stats, io
from matplotlib import pyplot

import time

def rates(accept, reject, threshold, greater=True):
    if greater:
        GA = np.sum(accept >= threshold)
        FA = np.sum(reject >= threshold)
    else:
        GA = np.sum(accept <= threshold)
        FA = np.sum(reject <= threshold)
    GAR = GA / len(accept)
    FAR = FA / len(reject)
    FRR = 1 - GAR
    return (GAR, FAR, FRR)


def list_rates(accept, reject, greater=True):
    all_scores = np.append(accept, reject)
    start, stop = all_scores.min(), all_scores.max()
    GARs, FARs, FRRs, thresholds = [], [], [], []
    for threshold in np.linspace(start, stop, num=200):
        GAR, FAR, FRR = rates(accept, reject, threshold, greater=greater)
        GARs.append(GAR)
        FARs.append(FAR)
        FRRs.append(FRR)
        thresholds.append(threshold)
    return (GARs, FARs, FRRs, thresholds)


def calculate_eer(accept, reject, greater=True):
    all_scores = np.append(accept, reject)
    threshold = all_scores.min()
    step = all_scores.max() - all_scores.min()
    if not greater:
        step = -step
    precision = 0.00001
    FAR, FRR = 0, 1
    while abs(step) > precision and FAR != FRR:
        GAR, FAR, FRR = rates(accept, reject, threshold, greater=greater)
        print("Gar: %f, Far: %f, Frr: %f" % (GAR, FAR, FRR))
        if (FAR >= FRR):
            threshold += step
        elif (FAR < FRR):
            threshold -= step
        step /= 2
    return (FRR + FAR) / 2, threshold


def plot_curves(accept, reject, title, greater=True, show=False,
                legendloc="best"):
    '''
    Plot ROC, FAR / GAR, and KDE curves
    '''
    numcolors = 9
    matplotlib.rcParams['axes.prop_cycle'] = \
        matplotlib.cycler(color=['#377eb8', '#ff7f00', '#4daf4a',
                                 '#f781bf', '#a65628', '#984ea3',
                                 '#999999', '#e41a1c', '#dede00',
                                 '#000000'][:numcolors])

    GARs, FARs, FRRs, thresholds = list_rates(accept, reject, greater=greater)
    eer, threshold = calculate_eer(accept, reject, greater=greater)
    accept_kernel = sp.stats.gaussian_kde(accept)
    reject_kernel = sp.stats.gaussian_kde(reject)
    maxval = np.max([np.max(accept), np.max(reject)])
    minval = np.min([np.min(accept), np.min(reject)])

    pyplot.figure(figsize=(15,4), dpi=300)
    pyplot.subplots_adjust(
        left = 0.05,
        right = 0.95,
        top = 0.9,
        bottom = 0.1,
        wspace = 0.2
    )
    # ROC curve
    pyplot.subplot(131)
    pyplot.title("ROC Curve")
    pyplot.plot(FARs, GARs, "C3")
    pyplot.plot([0,1], [0,1], "C6", alpha=0.2)
    pyplot.xlabel("False Accept Rate")
    pyplot.ylabel("Genuine Accept Rate")

    # FAR/FRR curve
    pyplot.subplot(132)
    pyplot.title("FAR / FRR")
    pyplot.locator_params(axis='x', nbins=7)
    pyplot.plot(thresholds, FARs, "--C7", label="FAR")
    pyplot.plot(thresholds, FRRs, "C0", label="FRR")
    pyplot.annotate(
        "EER = (%f , %f)" % (threshold, eer),
        xycoords = "data",
        xy = (threshold, eer),
        textcoords = "axes fraction",
        xytext = (0.5, 0.7),
        horizontalalignment = "center",
        verticalalignment = "top",
        alpha = 0.8,
        arrowprops = dict(
            facecolor = "black",
            alpha = 0.3,
            width = 0.2,
            headwidth = 6,
            shrink = 0.05
        )
    )
    print("Annotating plot with EER %f, threshold %f" % (eer, threshold))
    pyplot.xlabel("Threshold")
    pyplot.ylabel("Error rate")
    pyplot.legend(numpoints = 1, fancybox = True, prop = dict(size = 12),
                  loc = legendloc)

    # KDE curve
    pyplot.subplot(133)
    pyplot.title("KDE curve")
    pyplot.locator_params(axis='x', nbins=7)
    x = np.linspace(minval, maxval, 500)
    pyplot.plot(x, accept_kernel(x), "C2", label="Genuine")
    pyplot.plot(x, reject_kernel(x), "--C1", label="Manipulated")
    pyplot.xlabel("Score")
    pyplot.ylabel("Density")
    pyplot.yticks([])
    pyplot.legend(numpoints = 1, fancybox = True, prop = dict(size = 12),
                  loc = legendloc)

    if show:
        pyplot.show()
    else:
        current_time = time.strftime("%Y-%m-%d-%H-%M-%S")
        file_name = title + " " + current_time + ".pdf"
        pyplot.savefig(file_name)


