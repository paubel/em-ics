#!/usr/bin/env python3

import argparse
import numpy as np
import scipy as sp
import functools
from scipy import stats
from scipy import signal
from matplotlib import pyplot
from collections import defaultdict

from detect_peaks import detect_peaks


def memoize(filename, function_closure, force_generation=False, verbose=False):
    # Dirty memoization to disk.
    # function_closure should be a callable object that returns the
    # result to be memoized *as is*. Results will be written to filename
    # using np.savez.
    # Every function that is memoized in this way should return its values
    # as a dictionary.
    generate = force_generation
    if not force_generation:
        try:
            resultfile = np.load(filename)
        except IOError:
            if verbose:
                print("No %s present yet. Generating." % filename)
            generate = True

    if generate:
        resultdict = function_closure()
        np.savez(filename, **resultdict)
    else:
        if verbose:
            print("Loaded result from file %s" % filename)
        with resultfile:
            resultdict = dict(resultfile)

    return resultdict


def printTrsMetadata(ts):
    print("Number of traces:\t%d" % ts._numberOfTraces)
    print("Samples per trace:\t%d" % ts._numberOfSamplesPerTrace)
    print("Samples datatype:\t%s" % ts._npSampleCoding)
    print("Data bytes:\t\t%d" % ts._dataSpace)
    print("Trace block size:\t%d bytes" % ts._traceBlockSpace)
    print("Header size:\t\t%d bytes" % ts._traceBlockOffset)


def normalize(trace):
    return trace / np.max(np.abs(trace), axis=0)


def shiftdown(trace):
    if np.result_type(trace) == np.int16:
        if np.any(np.left_shift(trace, 8)):
            raise ValueError("Precision would be lost by right-shifting!")
        print("Shorts being shifted to chars")
        trace = np.right_shift(trace, 8).astype("int8")
    elif np.result_type(trace) == np.int8:
        pass
    else:
        raise ValueError("Unhandled result type: %s" % np.result_type(nptrace))
    return trace


def train(func, templatestring, filebase, tracesets, tracefilters, start,
          numsamples, setsize=None, verbose=False):
    # func should be one of median, average, or a compatible function that goes
    # through train.
    # templatestring should be the corresponding return-string ("average",
    # "median", or similar)
    returnfile = "%s %s.npz" % (filebase, templatestring)
    returnfunc = functools.partial(_train, func, templatestring, tracesets,
                                    tracefilters, start, numsamples, setsize)
    returndict = memoize(returnfile, returnfunc, verbose=verbose)

    if len(tracesets) != returndict["filternum"]:
        raise ValueError("Incompatibility between number of tracesets and "
                            "number of filters on %s!" % returnfile)

    trainingmasks = []
    trainingfilters = []
    matchingfilters = []
    for i in range(returndict["filternum"]):
        trainingmasks.append(returndict["trainingmasks_%d" % i])
        trainingfilters.append(returndict["trainingfilters_%d" % i])
        matchingfilters.append(returndict["matchingfilters_%d" % i])

    return {templatestring : returndict[templatestring],
            "trainingmasks" : trainingmasks,
            "trainingfilters" : trainingfilters,
            "matchingfilters" : matchingfilters}


def _train(func, templatestring, tracesets, tracefilters, start, numsamples,
           skip=0, stride=10, setsize=None):
    trainingmasks = []
    trainingfilters = []
    matchingfilters = []
    end = None
    if setsize:
        end = setsize * stride + skip
    for tracefilter in tracefilters:
        trainingmask = np.zeros(shape=len(tracefilter), dtype=bool)
        trainingmask[skip:end:stride] = True
        trainingmasks.append(trainingmask)
        trainingfilters.append(tracefilter[trainingmask])
        matchingfilters.append(tracefilter[np.invert(trainingmask)])

    templatedict = func(tracesets, trainingfilters, start, numsamples)

    returndict = {templatestring : templatedict[templatestring],
                  "filternum" : len(trainingfilters)}
    for i in range(returndict["filternum"]):
        returndict["trainingmasks_%d" % i] = np.array(trainingmasks[i])
        returndict["trainingfilters_%d" % i] = np.array(trainingfilters[i])
        returndict["matchingfilters_%d" % i] = np.array(matchingfilters[i])

    return returndict


def average(tracesets, tracefilters, start, numsamples, normalize=False):
    # start is number of samples to be *added* to the alignment offset.
    # so in most cases, we'd want a negative start.
    if normalize:
        sumtrace = np.zeros(shape=numsamples, dtype = "float64")
    else:
        sumtrace = np.zeros(shape=numsamples, dtype = "int64")

    numtraces = 0
    for ts, tracefilter in zip(tracesets, tracefilters):
        if not np.any(tracefilter):
            raise ValueError("Empty tracefilter")
        numtraces += len(tracefilter)
        for i, offset in tracefilter:
            trace = ts.getTrace(i)
            nptrace = trace._samples[offset + start : offset + start + numsamples]
            nptrace = shiftdown(nptrace)
            if normalize:
                nptrace = normalize(nptrace)
            sumtrace += nptrace

    avgtrace = sumtrace / numtraces
    return {"average" : avgtrace}


def histmedian(tracesets, tracefilters, start, numsamples):
    # Histogram-based median. Slow, but very low in memory requirements because
    # it exploits the fact that the inputs have a fixed number of values.

    # start is number of samples to be *added* to the alignment offset.
    # so in most cases, we'd want a negative start.
    histtrace = np.zeros(shape=(256, numsamples), dtype="int32")
    numtraces = 0
    for ts, tracefilter in zip(tracesets, tracefilters):
        if not np.any(tracefilter):
            raise ValueError("Empty tracefilter")
        numtraces += len(tracefilter)

    #subarr = np.transpose(np.resize(np.array(range(-128, 128)), (numsamples, 256)))
    #for i, offset in tracefilter:
    #    trace = ts.getTrace(i)
    #    nptrace = trace._samples[offset - 500 : offset + 3000]
    #    nptrace = np.right_shift(nptrace, 8)
    #    nptrace = np.resize(nptrace, (256, numsamples))
    #    nptrace -= subarr
    #    nptrace = np.logical_not(nptrace).astype(int)
    #    histtrace += nptrace
        for i, offset in tracefilter:
            trace = ts.getTrace(i)
            nptrace = trace._samples[offset + start : offset + start + numsamples]
            nptrace = shiftdown(nptrace)

            for j, sample in enumerate(nptrace):
                histtrace[sample + 128][j] += 1

    # Now, get the median trace from the histogram.
    histsums = np.transpose(np.cumsum(histtrace, axis=0))
    npmedian = np.empty(numsamples, dtype="int8")
    for i, histsum in enumerate(histsums):
       median = [j for j, v in enumerate(histsum >= len(tracefilter)/2) if v][0]
       npmedian[i] = median - 128

    return {"median" : npmedian}


def median(tracesets, tracefilters, start, numsamples, normalize=False):
    # start is number of samples to be *added* to the alignment offset.
    # so in most cases, we'd want a negative start.
    # This builds an array of all traces we take the median of, so be careful
    # about memory reqs.
    numtraces = sum(map(len, tracefilters))
    if normalize:
        aligned_traces = np.zeros(shape=(numtraces, numsamples), dtype = "float64")
    else:
        aligned_traces = np.zeros(shape=(numtraces, numsamples), dtype = "int8")

    ioff = 0
    for ts, tracefilter in zip(tracesets, tracefilters):
        if not np.any(tracefilter):
            raise ValueError("Empty tracefilter")
        for i, (tracenum, offset) in enumerate(tracefilter):
            trace = ts.getTrace(tracenum)
            nptrace = trace._samples[offset + start : offset + start + numsamples]
            nptrace = shiftdown(nptrace)
            if normalize:
                nptrace = normalize(nptrace)
            aligned_traces[i + ioff] = nptrace
        ioff += len(tracefilter)

    # Now, get the median trace.
    mediantrace = np.median(aligned_traces, axis=0)
    return {"median" : mediantrace}


def _correlate(traces, trace):
    corrs = np.zeros(shape=(traces.shape[0]), dtype=np.float64)
    for i, target in enumerate(traces):
        corrs[i] = np.max(sp.signal.correlate(trace, target, mode='valid'))
    return corrs


def alignedcorrs(ts, tracefilter, start, numsamples, template):
    if not np.any(tracefilter):
        return

    if template.shape[-1] != numsamples:
        print(template.shape)
        raise ValueError("Incompatible template shape with trace slice")

    if len(template.shape) == 1:
        template = np.reshape(template, (1, -1))
    elif len(template.shape) > 2:
        raise ValueError("Unable to handle templates matrices with more than 2 dimensions")

    corrs = np.zeros(shape=(len(tracefilter), template.shape[0]), dtype=np.float64)
    for i, (tracenum, offset) in enumerate(tracefilter):
        trace = ts.getTrace(tracenum)
        nptrace = trace._samples[offset + start : offset + start + numsamples]
        nptrace = shiftdown(nptrace)
        corrs[i] = _correlate(template, nptrace)
    return {"corrs" : corrs}


def _sadint8(traces, trace):
    # Use overflow behaviour of two's complement to get efficient results.
    signs = np.uint8(traces < trace) * 254 + 1
    traces = traces.view(dtype=np.uint8)
    trace = trace.view(dtype=np.uint8)
    diffs = traces - trace
    return np.sum(diffs*signs, axis=1)


def _sad(traces, trace):
    # Be careful when traces is of a datatype of equal size as trace.
    return np.sum(np.abs(traces - trace), axis=1)


def alignedsads(ts, tracefilter, start, numsamples, template):
    if not np.any(tracefilter):
        return

    if template.shape[-1] != numsamples:
        print(template.shape)
        raise ValueError("Incompatible template shape with trace slice")

    if len(template.shape) == 1:
        template = np.reshape(template, (1, -1))
    elif len(template.shape) > 2:
        raise ValueError("Unable to handle templates matrices with more than 2 dimensions")

    if np.result_type(template) == np.int8 and ts._npSampleCoding == "int8":
        sads = np.zeros(shape=(len(tracefilter), template.shape[0]), dtype=np.uint8)
        sadfunc = _sadint8
    else:
        sads = np.zeros(shape=(len(tracefilter), template.shape[0]), dtype=np.float64)
        sadfunc = _sad
    for i, (tracenum, offset) in enumerate(tracefilter):
        trace = ts.getTrace(tracenum)
        nptrace = trace._samples[offset + start : offset + start + numsamples]
        nptrace = shiftdown(nptrace)
        sads[i] = sadfunc(template, nptrace)

    return {"sads" : sads}


