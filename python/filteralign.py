#!/usr/bin/env python3

import numpy as np
import scipy as sp
from scipy import stats
from collections import defaultdict

from pytrs import Trace as trs
from detect_peaks import detect_peaks
from emutil import normalize

def filter_peaks(ts, verbose=False):
    bad_traces = []
    good_traces = []
    for i in range(0, ts._numberOfTraces):
        trace = ts.getTrace(i)
        nptrace = trace._samples
        nptrace = normalize(nptrace)
        peaks = detect_peaks(nptrace, mph=.50, mpd=250, show=False)
        # compute distances between adjacent peaks
        distances = np.diff(peaks)
        # find valleys larger than 2500 samples
        valleypeaks = [i for i, v in enumerate(distances >= 2750) if v]
        valleys = [distances[i] for i in valleypeaks]

        startpeak = 500
        if valleypeaks:
            startpeak = peaks[valleypeaks[0]+1]

        # program-specific: we know we want to see three such valleys.
        if len(valleys) != 3:
            if verbose:
                print("Trace %d does not look correct" % i)
                #detect_peaks(nptrace, mph=.50, mpd=250, show=True)
            bad_traces.append((i, startpeak))
            continue

        # we also know that the first valley should be no larger than 4000
        # samples and the second valley shouldn't be smaller than 5500 samples.
        # We don't really care about the third valley because that's beyond the
        # point of interest of this trace.
        if valleys[0] > 4000 or valleys[1] < 5500:
            if verbose:
                print("Trace %d has deviating valleys" % i)
                print(valleys)
                #detect_peaks(nptrace, mph=.50, mpd=250, show=True)
            bad_traces.append((i, startpeak))
            continue

        # Count the number of peaks between first and second valley with large
        # amplitude. Outliers imply interrupt.
        numpeaks = valleypeaks[1] - valleypeaks[0]
        if numpeaks not in [2, 3, 4]:
            if verbose:
                print("Trace %d has an erroneous number of peaks: %d" %
                      (i, numpeaks))
                #detect_peaks(nptrace, mph=.50, mpd=250, show=True)
            bad_traces.append((i, startpeak))
            continue

        good_traces.append((i, startpeak))

    return {"good" : np.array(good_traces), "bad" : np.array(bad_traces)}


if __name__ == "__main__":
    import argparse
    from matplotlib import pyplot
    parser = argparse.ArgumentParser(description="Use peak finding to remove outliers caused by interrupts from tracesets and align remaining traces.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        required=False, help="be verbose in output")
    parser.add_argument("-pg", "--plotgood", action="store_true",
                        required=False, help="plot the first 5000 traces of "
                                             "the filtered and aligned good "
                                             "traces in blocks of 1000 traces "
                                             "each")
    parser.add_argument("-pb", "--plotbad", action="store_true",
                        required=False, help="plot the first 5000 traces of "
                                             "the filtered bad traces "
                                             "in blocks of 1000 traces "
                                             "each")
    parser.add_argument("filename", help="traceset file name")
    args = parser.parse_args()

    ts = trs.TraceSet()
    ts.open(args.filename)
    filterdict = filter_peaks(ts, args.verbose)
    good_traces = filterdict["good"]
    bad_traces = filterdict["bad"]
    if args.verbose:
        print("Filtered %d traces from %d traces" % (len(bad_traces),
                                                     ts._numberOfTraces))

    if args.plotbad:
        for i in range(0, 5000, 1000):
            pyplot.figure(figsize=(10,6))
            for j, offset in bad_traces[i : i+1000]:
                trace = ts.getTrace(j)
                nptrace = trace._samples[offset - 500 : offset + 3000]
                pyplot.plot(nptrace)
            pyplot.title("Bad traces, block %d to %d" % (i, i + 1000))
        pyplot.show()

    if args.plotgood:
        for i in range(0, 5000, 1000):
            pyplot.figure(figsize=(10,6))
            for j, offset in good_traces[i : i+1000]:
                trace = ts.getTrace(j)
                nptrace = trace._samples[offset - 500 : offset + 3000]
                pyplot.plot(nptrace)
            pyplot.title("Good traces, block %d to %d" % (i, i + 1000))
        pyplot.show()

