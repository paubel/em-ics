#!/usr/bin/env python3

import argparse
import numpy as np
import scipy as sp
import functools
import time
import os, sys

from multiprocessing import Pool

from pytrs import Trace as trs

from emutil import average, median, histmedian, printTrsMetadata, normalize
from emutil import train, memoize
from emutil import alignedsads, alignedcorrs

from filteralign import filter_peaks

from anutil import plot_curves


def populate_tracesets(folder, programtypes, inputs, verbose=False):
    filebases = {}
    tracesets = {}
    for programtype in programtypes:
        filebases[programtype] = []
        tracesets[programtype] = []

        for i in range(inputs):
            filebase = "%sPLC waterlevel %s - %d" % (folder, programtype, i)
            ts = trs.TraceSet()
            ts.open("%s.trs" % filebase)
            if verbose:
                print(filebase)
            if veryverbose:
                printTrsMetadata(ts)
            filebases[programtype].append(filebase)
            tracesets[programtype].append(ts)
    return tracesets, filebases


def populate_setfilters(tracesets, filebases, verbose=False):
    setfilters = {}
    for programtype in tracesets.keys():
        setfilters[programtype] = []

        for i, ts in enumerate(tracesets[programtype]):
            setfilterfile = "%s setfilter.npz" % filebases[programtype][i]
            setfilterfunc = functools.partial(filter_peaks, ts, verbose=verbose)
            setfilterdict = memoize(setfilterfile, setfilterfunc, verbose=verbose)
            good_traces = setfilterdict["good"]
            bad_traces = setfilterdict["bad"]

            setfilters[programtype].append((good_traces, bad_traces))
            if verbose:
                print("Filtered %d traces from %d traces" % (len(bad_traces),
                                                             ts._numberOfTraces))
    return setfilters


def populate_medians_and_averages(tracesets, filebases, setfilters,
                                  verbose=False):
    # This function could be split, but by not doing so, we're actually being
    # helped a lot by the OS filesystem cache because instead of first computing
    # the averages for all inputs, then the medians for all inputs, we iterate
    # over the same file twice; so can make do with a smaller cache.
    separatemedians = {}
    combinedmedians = {}
    separateaverages = {}
    combinedaverages = {}

    for programtype in tracesets.keys():
        if verbose:
            print("For program %s" % programtype)
        ts = tracesets[programtype]
        good_traces, bad_traces = zip(*setfilters[programtype])

        filebase = "%s combined medians -" % filebases[programtype][0]
        combinedmedians[programtype] = train(median, "median", filebase, ts,
                                             good_traces, -150, 1500,
                                             verbose=verbose)

        filebase = "%s combined averages -" % filebases[programtype][0]
        combinedaverages[programtype] = train(average, "average", filebase, ts,
                                              good_traces, -150, 1500,
                                              verbose=verbose)

        separatemedians[programtype] = []
        separateaverages[programtype] = []
        for i, ts in enumerate(tracesets[programtype]):
            if verbose:
                print("For input %d on program %s" % (i, programtype))

            good_traces, bad_traces = setfilters[programtype][i]
            filebase = "%s separate medians -" % filebases[programtype][i]
            curmedian = train(median, "median", filebase, [ts], [good_traces],
                              -150, 1500, verbose=verbose)
            filebase = "%s separate averages -" % filebases[programtype][i]
            curaverage = train(average, "average", filebase, [ts], [good_traces],
                               -150, 1500, verbose=verbose)
            separatemedians[programtype].append(curmedian)
            separateaverages[programtype].append(curaverage)

    return combinedmedians, separatemedians, combinedaverages, separateaverages


def generate_sads_and_corrs(filebases, setfilters, combinedmedians,
                            separatemedians, combinedaverages, separateaverages,
                            verbose=False):
    templates = []
    # 34 templates per program, for a total of 102 templates.
    # Layout:
    # combined average for programtype normal
    # combined median for programtype normal
    # 16 separate averages for programtype normal
    # 16 separate medians for programtype normal
    # repeated for programtype logic switched
    # repeated for programtype flipped 12
    for programtype in combinedaverages.keys():
        templates.append(combinedaverages[programtype]["average"])
        templates.append(combinedmedians[programtype]["median"])
        for curaverage in separateaverages[programtype]:
            templates.append(curaverage["average"])
        for curmedian in separatemedians[programtype]:
            templates.append(curmedian["median"])

    templates = np.array(templates)

    poolprocesses = 24
    with Pool(processes=poolprocesses) as pool:
        # Fix processor affinity priority based on number of workers.
        # Depends on workers being alive for the duration of the pool.
        affinities = [y for x in [
                                  range(14,17),
                                  range(1,3),
                                  range(0,1),
                                  range(3,14),
                                  range(17,28),
                                  ] for y in x]
        affinities = affinities[:poolprocesses]
        pool.map(pool_init, affinities, 1)
        for crossprogramtype in filebases.keys():
            for i, filebase in enumerate(filebases[crossprogramtype]):
                if verbose:
                    print("From program %s with input %d" % (crossprogramtype,
                                                             i))
                good_traces, bad_traces = setfilters[crossprogramtype][i]

                # Unfortunately, we cannot pass the traceset without reopening
                # it, because the BufferedReader contained is not picklable.
                pool.apply_async(run_algorithms_pooled, (filebase, good_traces,
                                                         bad_traces, templates),
                                 {}, None, print)
        pool.close()
        pool.join()
    if verbose:
        print("Done with multiprocessing.")


def run_algorithms_pooled(filebase, good_traces, bad_traces, templates):
    # This function is intended to be run from a multiprocessing Pool.
    # Will reopen the traceset file since they cannot be serialized. Note the
    # lack of return value; this is purely intended as a generation function.
    # Any use of the resulting dataset should be done in the main process again.
    if verbose:
        print("From %s" % (filebase))
    ts = trs.TraceSet()
    ts.open("%s.trs" % filebase)
    run_sads(filebase, ts, good_traces, bad_traces, templates, verbose=True)
    run_corrs(filebase, ts, good_traces, bad_traces, templates, verbose=True)


def run_sads(filebase, ts, good_traces, bad_traces, templates, verbose=False):
    sadfile = ("%s - sads to templates.npz" % filebase)
    sadfunc = functools.partial(alignedsads, ts, good_traces, -150,
                                1500, templates)
    return memoize(sadfile, sadfunc, verbose=verbose)


def run_corrs(filebase, ts, good_traces, bad_traces, templates, verbose=False):
    corrfile = ("%s - corrs to templates.npz" % filebase)
    corrfunc = functools.partial(alignedcorrs, ts, good_traces, -150,
                                1500, templates)
    return memoize(corrfile, corrfunc, verbose=verbose)


def pool_init(affinity):
    # Set affinity. Use by mapping an affinity mask into the pool with a
    # size of processes, and a map chunksize of 1.
    # Sleep 1 second to ensure the mapping actually happens correctly.
    os.sched_setaffinity(os.getpid(), [affinity])
    time.sleep(1)


def process_combined(filebases, verbose=False):
    accept = []
    programtype = "normal"
    accept_corrs = []
    for i in range(inputs):
        filebase = filebases[programtype][i]
        corrfile = ("%s - corrs to templates.npz" % filebase)
        corrfunc = None
        curcorr = memoize(corrfile, corrfunc)["corrs"]

        matchingmask = np.invert(combinedaverages[programtype]["trainingmasks"][i])
        accept_corrs.extend(curcorr[matchingmask, 0])

    reject_corrs = []
    programtype = "logic switched"
    print(programtype)
    for i in range(16):
        filebase = filebases[programtype][i]
        corrfile = ("%s - corrs to templates.npz" % filebase)
        corrfunc = None
        curcorr = memoize(corrfile, corrfunc)["corrs"]

        matchingmask = np.invert(combinedaverages[programtype]["trainingmasks"][i])
        reject_corrs.extend(curcorr[matchingmask, 0])

    plot_curves(accept_corrs, reject_corrs, "results: correlation: combined average normal performance against logic switched")
    #plot_curves(accept_corrs, reject_corrs, "results: correlation: combined average normal performance against logic switched", legendloc="center left")

    reject_corrs = []
    programtype = "high flipped 12"
    print(programtype)
    for i in range(16):
        filebase = filebases[programtype][i]
        corrfile = ("%s - corrs to templates.npz" % filebase)
        corrfunc = None
        curcorr = memoize(corrfile, corrfunc)["corrs"]

        matchingmask = np.invert(combinedaverages[programtype]["trainingmasks"][i])
        reject_corrs.extend(curcorr[matchingmask, 0])

    plot_curves(accept_corrs, reject_corrs, "results: correlation: combined average normal performance against high flipped 12", greater=False)
    #plot_curves(accept_corrs, reject_corrs, "results: correlation: combined average normal performance against high flipped 12", greater=False, legendloc="center left")


def calculate_distinguishing_rate(filebases, combinedmedians, separatemedians,
                                  combinedaverages, separateaverages,
                                  verbose=False):
    # First, the performance of the combined average
    # Distinguishing between PrA and PrB, and PrA and PrC
    #for i, programtype in enumerate(filebases.keys())):
    print("Distinguishing normal and logic switched")
    for i, programtype in enumerate(["normal", "logic switched"]):
        recognized_sads = 0
        recognized_corrs = 0
        numsads = 0
        numcorrs = 0
        for j, filebase in enumerate(filebases[programtype]):
            sadfile = "%s - sads to templates.npz" % filebase
            corrfile = "%s - corrs to templates.npz" % filebase
            cursad = memoize(sadfile, None)["sads"]
            curcorr = memoize(corrfile, None)["corrs"]

            matchingmask = \
                np.invert(combinedaverages[programtype]["trainingmasks"][j])
            # Combined average distance is at position 0, 34, and 68.
            cursad = cursad[matchingmask]
            #cursad = cursad[:,[0,34,68]]
            cursad = cursad[:,[0,34]]
            numsads += len(cursad)
            curcorr = curcorr[matchingmask]
            #curcorr = curcorr[:,[0,34,68]]
            curcorr = curcorr[:,[0,34]]
            numcorrs += len(curcorr)

            # Now calculate the number of correctly recognized programs.
            cursadcount = np.sum(np.argmin(cursad, axis=1) == i)
            recognized_sads += cursadcount
            curcorrcount = np.sum(np.argmax(curcorr, axis=1) == i)
            recognized_corrs += curcorrcount
        sadrate = recognized_sads / numsads
        corrrate = recognized_corrs / numcorrs

        print("SAD distinguishing rate for program %s, combined averaging: %f" %
              (programtype, sadrate))
        print("Corr distinguishing rate for program %s, combined averaging: %f" %
              (programtype, corrrate))


    print("Distinguishing normal and high flipped 12")
    for i, programtype in enumerate(["normal", "high flipped 12"]):
        recognized_sads = 0
        recognized_corrs = 0
        numsads = 0
        numcorrs = 0
        for j, filebase in enumerate(filebases[programtype]):
            sadfile = "%s - sads to templates.npz" % filebase
            corrfile = "%s - corrs to templates.npz" % filebase
            cursad = memoize(sadfile, None)["sads"]
            curcorr = memoize(corrfile, None)["corrs"]

            matchingmask = \
                np.invert(combinedaverages[programtype]["trainingmasks"][j])
            # Combined average distance is at position 0, 34, and 68.
            cursad = cursad[matchingmask]
            #cursad = cursad[:,[0,34,68]]
            cursad = cursad[:,[0,68]]
            numsads += len(cursad)
            curcorr = curcorr[matchingmask]
            #curcorr = curcorr[:,[0,34,68]]
            curcorr = curcorr[:,[0,68]]
            numcorrs += len(curcorr)

            # Now calculate the number of correctly recognized programs.
            cursadcount = np.sum(np.argmin(cursad, axis=1) == i)
            recognized_sads += cursadcount
            curcorrcount = np.sum(np.argmax(curcorr, axis=1) == i)
            recognized_corrs += curcorrcount
        sadrate = recognized_sads / numsads
        corrrate = recognized_corrs / numcorrs

        print("SAD distinguishing rate for program %s, combined averaging: %f" %
              (programtype, sadrate))
        print("Corr distinguishing rate for program %s, combined averaging: %f" %
              (programtype, corrrate))

    # Distinguishing control flows within a program.
    print("Distinguishing control flows within normal")
    programtype = "normal"
    recognized_sads = 0
    recognized_corrs = 0
    numsads = 0
    numcorrs = 0
    # The profiles live at indices 2--18.
    indices = list(range(2,18))
    # Inputs 0--4 are the low flow
    for i in range(0, 5):
        filebase = filebases[programtype][i]
        sadfile = "%s - sads to templates.npz" % filebase
        corrfile = "%s - corrs to templates.npz" % filebase
        cursad = memoize(sadfile, None)["sads"]
        curcorr = memoize(corrfile, None)["corrs"]

        matchingmask = \
            np.invert(combinedaverages[programtype]["trainingmasks"][i])

        # Any match to the correct 5 inputs is okay, even if the input itself
        # is misclassified.
        cursad = cursad[matchingmask]
        cursad = cursad[:,indices]
        cursad = np.argmin(cursad, axis=1)
        numsads += len(cursad)
        cursadcount = np.sum(cursad < 5)
        recognized_sads += cursadcount

        curcorr = curcorr[matchingmask]
        curcorr = curcorr[:,indices]
        curcorr = np.argmax(curcorr, axis=1)
        numcorrs += len(curcorr)
        curcorrcount = np.sum(curcorr < 5)
        recognized_corrs += curcorrcount
    sadrate = recognized_sads / numsads
    corrrate = recognized_corrs / numcorrs
    print("SAD distinguishing rate for program %s, flow low, separate averaging: %f" %
            (programtype, sadrate))
    print("Corr distinguishing rate for program %s, flow low, separate averaging: %f" %
            (programtype, corrrate))

    recognized_sads = 0
    recognized_corrs = 0
    numsads = 0
    numcorrs = 0
    # Inputs 5--10 are the mid flow
    for i in range(5, 11):
        filebase = filebases[programtype][i]
        sadfile = "%s - sads to templates.npz" % filebase
        corrfile = "%s - corrs to templates.npz" % filebase
        cursad = memoize(sadfile, None)["sads"]
        curcorr = memoize(corrfile, None)["corrs"]

        matchingmask = \
            np.invert(combinedaverages[programtype]["trainingmasks"][i])

        # Any match to the correct 6 inputs is okay, even if the input itself
        # is misclassified.
        cursad = cursad[matchingmask]
        cursad = cursad[:,indices]
        cursad = np.argmin(cursad, axis=1)
        numsads += len(cursad)
        # Take the number of indices in the range 5--10
        cursadcount = np.sum(cursad >= 5)
        cursadcount -= np.sum(cursad > 10)
        recognized_sads += cursadcount

        curcorr = curcorr[matchingmask]
        curcorr = curcorr[:,indices]
        curcorr = np.argmax(curcorr, axis=1)
        numcorrs += len(curcorr)
        curcorrcount = np.sum(curcorr >= 5)
        curcorrcount -= np.sum(curcorr > 10)
        recognized_corrs += curcorrcount
    sadrate = recognized_sads / numsads
    corrrate = recognized_corrs / numcorrs
    print("SAD distinguishing rate for program %s, flow mid, separate averaging: %f" %
            (programtype, sadrate))
    print("Corr distinguishing rate for program %s, flow mid, separate averaging: %f" %
            (programtype, corrrate))

    recognized_sads = 0
    recognized_corrs = 0
    numsads = 0
    numcorrs = 0
    # Inputs 11--15 are the high flow
    for i in range(11, 16):
        filebase = filebases[programtype][i]
        sadfile = "%s - sads to templates.npz" % filebase
        corrfile = "%s - corrs to templates.npz" % filebase
        cursad = memoize(sadfile, None)["sads"]
        curcorr = memoize(corrfile, None)["corrs"]

        matchingmask = \
            np.invert(combinedaverages[programtype]["trainingmasks"][i])

        # Any match to the correct 5 inputs is okay, even if the input itself
        # is misclassified.
        cursad = cursad[matchingmask]
        cursad = cursad[:,indices]
        cursad = np.argmin(cursad, axis=1)
        numsads += len(cursad)
        cursadcount = np.sum(cursad >= 11)
        recognized_sads += cursadcount

        curcorr = curcorr[matchingmask]
        curcorr = curcorr[:,indices]
        curcorr = np.argmax(curcorr, axis=1)
        numcorrs += len(curcorr)
        curcorrcount = np.sum(curcorr >= 11)
        recognized_corrs += curcorrcount
    sadrate = recognized_sads / numsads
    corrrate = recognized_corrs / numcorrs
    print("SAD distinguishing rate for program %s, flow high, separate averaging: %f" %
            (programtype, sadrate))
    print("Corr distinguishing rate for program %s, flow high, separate averaging: %f" %
            (programtype, corrrate))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Full processing. Align traces using peak finding, compute SAD & XCORR, plot results.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        required=False, help="be verbose in output")
    parser.add_argument("-vv", "--veryverbose", action="store_true",
                        required=False, help="be even more verbose")
    args = parser.parse_args()

    verbose = args.verbose or args.veryverbose
    veryverbose = args.veryverbose

    folder = "/home/pol/EM-data/2017-05-27/"
    #folder = "/home/macgyver/radboud/phd-student/research/EM/data/"


    programtypes = ["normal", "logic switched", "high flipped 12"]
    inputs = 16

    tracesets, filebases = populate_tracesets(folder, programtypes, inputs,
                                              verbose=verbose)

    setfilters = populate_setfilters(tracesets, filebases, verbose=verbose)

    if verbose:
        print("Calculating medians and averages")

    combinedmedians, separatemedians, combinedaverages, separateaverages = \
        populate_medians_and_averages(tracesets, filebases, setfilters,
                                      verbose=verbose)

    if verbose:
        print("Calculating SADs and correlations")

    generate_sads_and_corrs(filebases, setfilters, combinedmedians,
                            separatemedians, combinedaverages, separateaverages,
                            verbose=verbose)

    print("Processing results")

    calculate_distinguishing_rate(filebases, combinedmedians, separatemedians,
                                  combinedaverages, separateaverages,
                                  verbose=verbose)

    process_combined(filebases, verbose=verbose)

    accept_sads = []
    programtype = programtypes[0]
    print(programtype)
    for i in range(16):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(combinedaverages[programtype]["trainingmasks"][i])
        accept_sads.extend(cursad[matchingmask, 0])

    reject_sads = []
    programtype = programtypes[1]
    print(programtype)
    for i in range(16):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(combinedaverages[programtype]["trainingmasks"][i])
        reject_sads.extend(cursad[matchingmask, 0])

    plot_curves(accept_sads, reject_sads, "results: SAD: combined average normal performance against logic switched", greater=False)
    #plot_curves(accept_sads, reject_sads, "results: SAD: combined average normal performance against logic switched", greater=False, legendloc="center right")

    reject_sads = []
    programtype = programtypes[2]
    print(programtype)
    for i in range(16):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(combinedaverages[programtype]["trainingmasks"][i])
        reject_sads.extend(cursad[matchingmask, 0])

    plot_curves(accept_sads, reject_sads, "results: SAD: combined average normal performance against high flipped 12", greater=False)
    #plot_curves(accept_sads, reject_sads, "results: SAD: combined average normal performance against high flipped 12", greater=False, legendloc="center right")


    accept_sads = []
    programtype = programtypes[0]
    print(programtype)
    for i in range(16):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(combinedmedians[programtype]["trainingmasks"][i])
        accept_sads.extend(cursad[matchingmask, 1])

    reject_sads = []
    programtype = programtypes[1]
    print(programtype)
    for i in range(16):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(combinedmedians[programtype]["trainingmasks"][i])
        reject_sads.extend(cursad[matchingmask, 1])

    plot_curves(accept_sads, reject_sads, "results: SAD: combined median normal performance against logic switched", greater=False)
    #plot_curves(accept_sads, reject_sads, "results: SAD: combined median normal performance against logic switched", greater=False, legendloc="center right")

    reject_sads = []
    programtype = programtypes[2]
    print(programtype)
    for i in range(16):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(combinedmedians[programtype]["trainingmasks"][i])
        reject_sads.extend(cursad[matchingmask, 1])

    plot_curves(accept_sads, reject_sads, "results: SAD: combined median normal performance against high flipped 12", greater=False)
    #plot_curves(accept_sads, reject_sads, "results: SAD: combined median normal performance against high flipped 12", greater=False, legendloc="center right")


    accept_sads = []
    programtype = programtypes[0]
    print(programtype)
    for i in range(1):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(separateaverages[programtype][i]["trainingmasks"][0])
        accept_sads.extend(cursad[matchingmask, 2])

    reject_sads = []
    programtype = programtypes[1]
    print(programtype)
    for i in range(1):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(separateaverages[programtype][i]["trainingmasks"][0])
        reject_sads.extend(cursad[matchingmask, 2])

    plot_curves(accept_sads, reject_sads, "results: SAD: separate average normal performance against logic switched", greater=False)
    #plot_curves(accept_sads, reject_sads, "results: SAD: separate average normal performance against logic switched", greater=False, legendloc="center right")

    reject_sads = []
    programtype = programtypes[2]
    print(programtype)
    for i in range(1):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(separateaverages[programtype][i]["trainingmasks"][0])
        reject_sads.extend(cursad[matchingmask, 2])

    plot_curves(accept_sads, reject_sads, "results: SAD: separate average normal performance against high flipped 12", greater=False)
    #plot_curves(accept_sads, reject_sads, "results: SAD: separate average normal performance against high flipped 12", greater=False, legendloc="center right")


    accept_sads = []
    programtype = programtypes[0]
    print(programtype)
    for i in range(1):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(separatemedians[programtype][i]["trainingmasks"][0])
        accept_sads.extend(cursad[matchingmask, 18])

    reject_sads = []
    programtype = programtypes[1]
    print(programtype)
    for i in range(1):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(separatemedians[programtype][i]["trainingmasks"][0])
        reject_sads.extend(cursad[matchingmask, 18])

    plot_curves(accept_sads, reject_sads, "results: SAD: separate median normal performance against logic switched", greater=False)
    #plot_curves(accept_sads, reject_sads, "results: SAD: separate median normal performance against logic switched", greater=False, legendloc="center right")

    reject_sads = []
    programtype = programtypes[2]
    print(programtype)
    for i in range(1):
        filebase = filebases[programtype][i]
        sadfile = ("%s - sads to templates.npz" % filebase)
        sadfunc = None
        cursad = memoize(sadfile, sadfunc)["sads"]

        matchingmask = np.invert(separatemedians[programtype][i]["trainingmasks"][0])
        reject_sads.extend(cursad[matchingmask, 18])

    plot_curves(accept_sads, reject_sads, "results: SAD: separate median normal performance against high flipped 12", greater=False)
    #plot_curves(accept_sads, reject_sads, "results: SAD: separate median normal performance against high flipped 12", greater=False, legendloc="center right")


