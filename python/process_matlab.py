#!/usr/bin/env python3

import argparse
import numpy as np
import scipy as sp
from scipy import stats, io
import os, sys

from pytrs import Trace as trs

from anutil import plot_curves

def export_filtered_set(folder, programs, inputs, start, numsamples,
                        verbose=False):
    for program in programs:
        export = []
        filebase = "%sPLC waterlevel %s" % (folder, program)
        for i in inputs:
            if verbose:
                print("Exporting for %s" % filebase)
            filebaseindexed = "%s - %d" % (filebase, i)
            ts = trs.TraceSet()
            ts.open("%s.trs" % filebaseindexed)
            setfilterfile = "%s setfilter.npz" % filebaseindexed
            try:
                resultfile = np.load(setfilterfile)
            except IOError:
                if verbose:
                    print("No %s present yet. Generate before running export." %
                          setfilterfile)
                    return
            with resultfile:
                resultdict = dict(resultfile)
            good_traces = resultdict["good"]

            num_traces = 50000

            if len(good_traces) < num_traces:
                print("Not enough traces in %s" % filebaseindexed)
                return

            exporttraces = np.zeros(shape=(len(good_traces), numsamples), dtype=np.int8)
            for j, (tracenum, offset) in enumerate(good_traces):
                if verbose:
                    print("Processing trace %d with offset %d" % (tracenum, offset))
                trace = ts.getTrace(tracenum)
                nptrace = trace._samples[offset + start : offset + start + numsamples]
                if np.result_type(nptrace) != np.int8:
                    print("Unexpected datatype in %s" % filebaseindexed)
                    return
                exporttraces[j] = nptrace

            export.append(exporttraces[:num_traces])

            with open("%s - matlab.trs" % filebaseindexed, "wb") as output_file:
                output_file.write(exporttraces[:num_traces])

        # Combined export
        num_traces = 6000
        with open("%s - matlab combined.trs" % filebase, "wb") as output_file:
            for dataset in export:
                # Write the training data
                # Remember to fix the matlab train set size accordingly.
                output_file.write(dataset[:1000])
            for dataset in export:
                # Write the testset data
                output_file.write(dataset[1000:num_traces])

def load_matlab_scores(filename, verbose=False):
    mc = sp.io.loadmat(filename)
    return mc['scores']


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Export traces for use with the matlab scripts, import & plot results.")
    parser.add_argument("-v", "--verbose", action="store_true",
                        required=False, help="be verbose in output")
    parser.add_argument("-vv", "--veryverbose", action="store_true",
                        required=False, help="be even more verbose")
    parser.add_argument("-e", "--export-for-matlab", action="store_true",
                        required=False, help="Export filtered and aligned "
                        "traces to files for matlab.")
    args = parser.parse_args()

    export = args.export_for_matlab
    verbose = args.verbose or args.veryverbose
    veryverbose = args.veryverbose

    folder = "/home/pol/EM-data/2017-05-27/matlab-results/"
    #folder = "/home/pol/radboud/phd-student/research/EM/data/"

    if export:
        programs = ["normal", "logic switched", "high flipped 12"]
        inputs = range(16)
        start = -150
        numsamples = 1500
        export_filtered_set(folder, programs, inputs, start, numsamples,
                            verbose=verbose)
        sys.exit(0)

    filename = "%scombined-normal-logicswitched.mat" % folder
    scores = load_matlab_scores(filename, verbose=verbose)
    accept = scores[:,0,0]
    reject = scores[:,1,0]

    title = "results: templating: normal performance against logic switched"
    plot_curves(accept, reject, title)
    #plot_curves(accept, reject, title, legendloc="center left")

    filename = "%scombined-normal-highflipped.mat" % folder
    scores = load_matlab_scores(filename, verbose=verbose)
    accept = scores[:,0,0]
    reject = scores[:,1,0]

    title = "results: templating: normal performance against high flipped"
    plot_curves(accept, reject, title)
    #plot_curves(accept, reject, title, legendloc="center left")

